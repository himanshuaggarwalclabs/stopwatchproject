//
//  ViewController.swift
//  myStopwatch
//
//  Created by Samar Singla on 15/01/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // variables
    var count = 0
    var hours = 0
    var second = 0
    var minute = 0
    var timer = NSTimer()
    var checktime = false
    
    // labels
    @IBOutlet weak var hoursDisplay: UILabel!
    
    @IBOutlet weak var minuteDisplay: UILabel!
    
    @IBOutlet weak var secDisplay: UILabel!
 
    @IBOutlet weak var minsecDisplay: UILabel!
    
    
    @IBAction func refreshButton(sender: AnyObject) {
        
        count = 0
        hours = 0
        second = 0
        minute = 0
        
        hoursDisplay.text = "00"
        minuteDisplay.text = "00"
        secDisplay.text = "00"
        minsecDisplay.text = ""
        // println("reset")
        timer.invalidate()
        checktime = false
                                                      // for Reset StopWatch
    }
    
    
    
    @IBAction func playButton(sender: AnyObject) {
        
        if checktime == false {
        
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector:Selector("updateValue"), userInfo: nil, repeats: true)
        }
        checktime = true
                                                 // for start StopWatch
    }
    
    // function for update time variables
    func updateValue() {
        
        count++
        
        if count > 99 {
            count = 0
            second++
        }
        if second > 59 {
            second = 0
            minute++
        }
        if minute > 59 {
            hours++
            minute = 0
        }
        
        if hours > 59 && minute > 59 && second > 59 && count > 99 {
            
            timer.invalidate()
        }
        
        hoursDisplay.text = "\(hours)"
        minuteDisplay.text = "\(minute)"
        secDisplay.text = "\(second)"
        minsecDisplay.text = "\(count)"
      
    }
    
    @IBAction func pauseButton(sender: AnyObject) {
       
       
         timer.invalidate()
         checktime = false
                                               // for pause StopWatch
    }
    override func viewDidLoad() {
       
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

